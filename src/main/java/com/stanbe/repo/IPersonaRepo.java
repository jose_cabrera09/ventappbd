package com.stanbe.repo;

import com.stanbe.model.Persona;

public interface IPersonaRepo extends IGenericRepo<Persona, Integer> {

}
