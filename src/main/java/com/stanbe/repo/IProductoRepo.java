package com.stanbe.repo;

import com.stanbe.model.Producto;

public interface IProductoRepo extends IGenericRepo<Producto, Integer> {

}
