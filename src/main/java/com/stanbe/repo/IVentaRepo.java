package com.stanbe.repo;

import com.stanbe.model.Venta;

public interface IVentaRepo extends IGenericRepo<Venta, Integer> {

}
