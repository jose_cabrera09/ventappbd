package com.stanbe.service;

import com.stanbe.model.Venta;

public interface IVentaService extends ICRUD<Venta, Integer> {

	Venta registrarVentaTransaccional(Venta venta);
	
}
