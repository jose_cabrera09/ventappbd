package com.stanbe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stanbe.model.Producto;
import com.stanbe.repo.IGenericRepo;
import com.stanbe.repo.IProductoRepo;
import com.stanbe.service.IProductoService;

@Service
public class ProductoServiceImpl extends CRUDImpl<Producto, Integer> implements IProductoService {

	@Autowired
	private IProductoRepo repo;
	
	@Override
	protected IGenericRepo<Producto, Integer> getRepo() {
		return this.repo;
	}

}
