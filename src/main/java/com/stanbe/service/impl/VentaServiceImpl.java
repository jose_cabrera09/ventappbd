package com.stanbe.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stanbe.model.Venta;
import com.stanbe.repo.IGenericRepo;
import com.stanbe.repo.IVentaRepo;
import com.stanbe.service.IVentaService;

@Service
public class VentaServiceImpl extends CRUDImpl<Venta, Integer> implements IVentaService {

	@Autowired
	private IVentaRepo repo;
	
	@Override
	protected IGenericRepo<Venta, Integer> getRepo() {
		return this.repo;
	}

	@Transactional
	@Override
	public Venta registrarVentaTransaccional(Venta venta) {
		
		venta.getDetalleVenta().forEach(dv -> dv.setVenta(venta));
		repo.save(venta);
		
		return venta;
	}
}
