package com.stanbe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stanbe.model.Persona;
import com.stanbe.repo.IGenericRepo;
import com.stanbe.repo.IPersonaRepo;
import com.stanbe.service.IPersonaService;

@Service
public class PersonaServiceImpl extends CRUDImpl<Persona, Integer> implements IPersonaService {

	@Autowired
	private IPersonaRepo repo;
	
	@Override
	protected IGenericRepo<Persona, Integer> getRepo() {
		return this.repo;
	}

}
